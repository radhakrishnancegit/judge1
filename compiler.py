import os,commands
import m_dbutils
import m_cplusplus,m_c
import time

def compile_and_run_submission(run): 
	Language = run["language"]
	if Language == "CPP":
		print "C++ Submission received, Id :%d" % (run["run_id"])
		m_cplusplus.compile_and_run(run)
	elif Language == "C":
		print "C Submission Received , Id :%d" % (run["run_id"])  
		m_c.compile_and_run(run)


#current_submissions = m_dbutils.query_submission_queue("COMPILATION_SUCCESS")
#while True:
current_submissions = m_dbutils.query_submission_queue("QUEUED")
if current_submissions != None :
	print current_submissions
	for run in current_submissions:	
		print run
		compile_and_run_submission(run)
# Sleep for 100ms before querying db

