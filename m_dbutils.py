import sys
import json
import StringConstants
from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import mapper, sessionmaker
import MySQLdb as sql

db = dict()
db["host"] = "localhost"
db["user"] = "root"
db["passwd"] = "wtpmjgda"
db["database"] = "hacktheinterview_test"
 
class Problem(object):
    pass
class Submission(object):
    pass
class Problem_Functions(object):
    pass
class Submission_unitrun(object):
    pass

def rows2dict(rows):
	res = []
	for row in rows:
		res.append(row.__dict__)
	return res

def loadSession():
    db = dict()
    db["user"]="root"
    db["password"]="wtpmjgda"
    db["host"]="localhost"
    db["db"]="hacktheinterview_test"
    engine = create_engine('mysql+mysqldb://%s:%s@%s/%s?charset=utf8'%(db["user"],db["password"],db["host"],db["db"]))
    metadata = MetaData(engine)
    
    mapper(Problem,Table('Problem', metadata, autoload=True))
    mapper(Submission,Table('Submission', metadata, autoload=True))
    mapper(Submission_unitrun,Table('Submission_unitrun', metadata, autoload=True))
    mapper(Problem_Functions,Table('Problem_Functions', metadata, autoload=True))
    
    Session = sessionmaker(bind=engine)
    session = Session()
    return session

sqlSession = loadSession()

def query_submission_queue(status):	
	rows = None
	rows = sqlSession.query(Submission).filter(Submission.status=="QUEUED").all()
	rows = rows2dict(rows)
	return rows

def query_main_code(problem_id,language):
	res = sqlSession.query(Problem_Functions).filter(Problem_Functions.problem_id==problem_id).all()
	res = rows2dict(res)
	if language == "C":
		return res[0]["c_main_code"]
	elif language == "CPP":
		return res[0]["cpp_main_code"]
	elif language == "JAVA":
		return res[0]["java_main_code"]
	else:
		return ""

def update_submission(run,result):
	if result["status"] not in StringConstants.SANDBOX:
		status = "DOOMED"
	else:
		status = StringConstants.SANDBOX_ERROR_CODE(result["status"])
	submission = sqlSession.query(Submission).filter(Submission.run_id==run["run_id"]).update({
		'status':result["status"],
		'time_taken':result["time_taken"],
		'exit_code':result["exit_code"],
		})
	sqlSession.commit()

def update_compilation_status(run,result,compiler_error_log):
	print compiler_error_log
	submission = sqlSession.query(Submission).filter(Submission.run_id).update({
		'status':result,
		'compiler_error_log':compiler_error_log,
		})
	sqlSession.commit()

def query_compilation_result(run_id):
	rows = None
	rows = sqlSession.query(Submission).filter(Submission.run_id==run_id).all()
	rows = rows2dict(rows)
	if len(rows) > 0:
		return rows[0]["status"]
	else :
		return None

def query_problem_info(problem_id):
	res = sqlSession.query(Problem).filter(Problem.problem_id==problem_id).all()
	res = rows2dict(res)
	return res[0]

def update_each_testrun(run,result):
	if result["status"] not in StringConstants.SANDBOX:
		status = "DOOMED"
	else:
		status = StringConstants.SANDBOX_ERROR_CODE(result["status"])

	newobject = Submission_unitrun()
	newobject.time_taken = result["time_taken"]
	newobject.output = result["output"]
	newobject.result = result["status"]
	newobject.exit_code = result["exit_code"]
	newobject.testcase_no = result["testcase_no"]
	newobject.Submission_id = run["run_id"]
	sqlSession.add(newobject)
	sqlSession.commit()

