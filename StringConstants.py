LIBSANDBOX = dict()
LIBSANDBOX["PD"] = "Pending in Judge"
LIBSANDBOX["OK"] = "ACCEPTED"
LIBSANDBOX["RF"] = "Restricted Function"
LIBSANDBOX["RT"] = "Run Time Error"
LIBSANDBOX["TL"] = "Time Limit Exceeded"
LIBSANDBOX["ML"] = "Memory Limit Exceeded"
LIBSANDBOX["OL"] = "Output Limit Exceeded"
LIBSANDBOX["AT"] = "Abnormal Termination"
LIBSANDBOX["IE"] = "Sandbox Internal Error"
LIBSANDBOX["BP"] = "Bad Policy"
LIBSANDBOX["WA"] = "Wrong Answer"

SANDBOX_CODE = ['PD', 'OK', 'RF', 'RT', 'TL', 'ML', 'OL', 'AT', 'IE', 'BP','WA']

def LIBSANDBOX_ERROR_CODE(status):
	return LIBSANDBOX[status]