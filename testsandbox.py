import os,commands,sys

def run_submission(f):

	command="/usr/bin/python "+f["sandboxlog"]+" "+f["time_limit"]+" "+\
				f["memory_limit"]+" "+f["binary"] + " < " + f["input"] + \
				" > " + f["output"] + " 2>" +f["sandboxlog"]	
	print command
	os.chroot(f["jail"])
	os.chdir(f["submission_directory"])
	os.system(command)

if __name__ == "__main__":
	argv = ['/usr/bin/python', 'testsandbox.py', '/home/radhakrish/jail', '/sandbox_c_cpp.py', 'submissions/1/a.out', 'submissions/1', '1', '256', 'submissions/1/sampleinput1.in', 'submissions/1/output1.out', 'submissions/1/log']
	print argv[0]
	f=dict()
	f["jail"] = argv[1]
	f["sandbox"] = argv[2]
	f["binary"] =argv[3]
	f["submission_directory"] = argv[4]
	f["time_limit"] = argv[5]
	f["memory_limit"] = argv[6]
	f["input"] = argv[7]
	f["output"] = argv[8]
	f["sandboxlog"] = argv[9]
	run_submission(f)


"""
"""