import os,commands,subprocess
import m_dbutils,StringConstants

# Number of lines to subtract when compilation error occurs :P
no_of_lines_to_subtract = 0

HOME = os.environ['HOME']
JAIL_LOCATION = os.path.join(HOME,"jail")
SUBMISSION_DIRECTORY = os.path.join(JAIL_LOCATION,"submissions")
MAXIMUM_OUTPUT_FILE_LIMIT = 1024*1024; # 1 MB 
IO_FOLDER = "data"
SANDBOX_WRAPPER = "/sandbox_c_cpp.py"

def preparesourcecode(run,driver_enabled):
	user_source_code = run["source_code"]
	main_code = m_dbutils.query_main_code(run["problem_id"],run["language"])
	delimiter = "USER_CODE_COMES_HERE"
	
	if driver_enabled == True:
		lines = main_code.split("\n")
		lines = map(lambda s: s.strip(), lines)
		line_no = lines.index("USER_CODE_COMES_HERE")
		global no_of_lines_to_subtract
		no_of_lines_to_subtract = line_no

	source_code = main_code.replace(delimiter,user_source_code)
	return source_code

def writetofile(content,source_file_path):
	file = open(source_file_path,"w")
	file.write(content)
	file.close()

def parse_libsandbox_result(filelocation):
	f = open(filelocation)
	lines = f.readlines()
	f.close()
	result = dict()
	if len(lines) == 3 :
		result["status"] = lines[0][7:9]
		result["time_taken"] = max(1,int(lines[1][4:]))
		result["memory_taken"] = int(lines[2][4:])*1024
		return result
	else :
		print 'Error while Parsing LibSandBox result '
		return result
	
def execute(run,problem_info):

	current_submission_directory = os.path.join(SUBMISSION_DIRECTORY,str(run["run_id"]))
	path_of_executable_binary = os.path.join(current_submission_directory,"a.out")
	INPUT_FOLDER_LOCATION = os.path.join(HOME,IO_FOLDER,str(run["problem_id"]))
	OUTPUT_FILE_TEMP_LOCATION  =  os.path.join(HOME,IO_FOLDER)

	finalresult = dict()
	cnt  = 1
	driver_enabled = problem_info["is_driver_enabled"]
	testcnt = 0

	if run["issubmission"] == True:
		testcnt = problem_info["test_cases_count"]
	else:
		testcnt = problem_info["sample_test_cases_count"]

	for i in range(testcnt):

		if run["issubmission"] == True:
			input_file_name = "input" + str(i+1)+".in"
		else:
			input_file_name = "sampleinput" + str(i+1)+".in"


		output_file_name = "output" + str(i+1) + ".out"

		# Copy inputfile to jail
		current_input_file_location = os.path.join(INPUT_FOLDER_LOCATION,input_file_name)
		os.system("cp %s %s"% (current_input_file_location,current_submission_directory))

		# Create result file if needed
		if driver_enabled == True:
			result_file_location = os.path.join(current_submission_directory,"result.txt")
			os.system("touch "+ result_file_location)
		
		# J_ = Relative to jail	
		J_submission_directory = os.path.join("submissions",str(run["run_id"]))
		J_input_file_location = os.path.join(J_submission_directory,input_file_name)
		J_output_file_location = os.path.join(J_submission_directory,output_file_name)
		J_sanbox_result_location = os.path.join(J_submission_directory,"log")
		J_binary = os.path.join(J_submission_directory,"a.out")

		# All args should be relative to jail 
		args = []
		args.append("/usr/bin/python")
		args.append("testsandbox.py")
		args.append(JAIL_LOCATION)
		args.append(SANDBOX_WRAPPER)
		args.append(J_binary)
		args.append(J_submission_directory)
		args.append(str(problem_info["time_limit_per_test_case"]))
		args.append(str(problem_info["memory_limit_per_test_case"]))
		args.append(J_input_file_location)
		args.append(J_output_file_location)
		args.append(J_sanbox_result_location)
		print args
		subprocess.call(args)

		result = parse_libsandbox_result(os.path.join(JAIL_LOCATION,J_sanbox_result_location))
		# Verify Wrong Answer 
		if result["status"] == "OK":
			# if you have to check PASS/FAIL
			if driver_enabled == True:
				file = open(result_file_location)
				temp = file.readlines()
				if len(temp) == 0 :
					result["status"] = "WA"
				if len(temp) > 0 and temp[0].rstrip() != "PASS" :
					result["status"] = "WA"
			# if you have to compare raw output file
			else:
				answer_output_file_location = os.path.join(OUTPUT_FILE_TEMP_LOCATION,str(run["problem_id"]),"output"+str(i+1)+".out")
				diffStatement = "diff --ignore-all-space --ignore-blank-lines %s %s " %(run_output_file_location,answer_output_file_location)
				diffOutput = commands.getoutput(diffStatement)
				if diffOutput != '':
					result["status"] = "WA"
		
		# output = std_output
		output_file_size = os.stat(os.path.join(JAIL_LOCATION,J_output_file_location)).st_size		
		f = open(os.path.join(JAIL_LOCATION,J_output_file_location))
		MAXIMUM_OUTPUT_FILE_LIMIT = 1024*100; # 100 kB 
		#print output_file_size
		output = f.read(min(MAXIMUM_OUTPUT_FILE_LIMIT,output_file_size))

		result["output"] = output
		result["status"] = StringConstants.LIBSANDBOX[result["status"]]
		result["testcase_no"] = cnt
		m_dbutils.update_each_testrun(run,result)
		finalresult = result
		
		if result["status"] != "ACCEPTED":
			break
		cnt+=1

	# Remove Directory
	command = "rm -rf %s" % (current_submission_directory)
	os.system(command)
	m_dbutils.update_submission(run,finalresult)

def compile(run,problem_info):
	# Create a Directory for the submission if not present

	current_submission_directory = os.path.join(SUBMISSION_DIRECTORY,str(run["run_id"]))
	mkdir_command = "mkdir -p " + current_submission_directory
	if not os.path.exists(current_submission_directory):
		os.system(mkdir_command)
	os.chdir(current_submission_directory)
	print current_submission_directory
	# Create the source File 

	source_code = preparesourcecode(run,problem_info["is_driver_enabled"])
	source_file_path = os.path.join(current_submission_directory,str(run["problem_id"])+".c")	
	writetofile(source_code,source_file_path)
	compiler_error_log_location = os.path.join(current_submission_directory,"compiler_error_log")
	# Compile the Source_code
	compile_command = "gcc -w -static -pipe -lm -s -fomit-frame-pointer -o %s %s 2>%s" %(current_submission_directory+"/a.out",source_file_path,compiler_error_log_location)
	temp = commands.getoutput(compile_command)

	# Adjust the lines if we have added some header for the user
	compiler_error_log=edit_compiler_log(compiler_error_log_location)
	
	if compiler_error_log == '':
		# Update DB saying 'COMPILATION SUCCESS'
		m_dbutils.update_compilation_status(run,"COMPILATION_SUCCESS"," ")
		return "COMPILATION_SUCCESS"
	else :
		
		# Update DB saying 'COMPILATION FAILURE'
		m_dbutils.update_compilation_status(run,"COMPILATION_ERROR",compiler_error_log)
		return "COMPILATION_ERROR"

def edit_compiler_log(compiler_error_log_location):

	global no_of_lines_to_subtract
	# Refer http://stackoverflow.com/questions/18022124/parsing-gcc-error-log
	awk_firstpart = "awk "
	awk_secondpart=   "'$4==\" warning\" || $4==\" note\" || $4==\" error\"{$2-=%s}1'" %(no_of_lines_to_subtract)
	awk_thirdpart = " FS=':' OFS=':' "
	awkcommand = awk_firstpart + awk_secondpart + awk_thirdpart + " " + compiler_error_log_location 
	edited_compiler_error_log = commands.getoutput(awkcommand)	
	return edited_compiler_error_log

def compile_and_run(run):
	
	problem_info = m_dbutils.query_problem_info(run["problem_id"])
	compilation_result= compile(run,problem_info)

	if compilation_result == "COMPILATION_SUCCESS":
		execute(run,problem_info)
