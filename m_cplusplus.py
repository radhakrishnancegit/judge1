import os,commands
import m_dbutils

# Number of lines to subtract when compilation error occurs :P
no_of_lines_to_subtract = 0

def preparesourcecode(run,driver_enabled):
	user_source_code = run["source_code"]
	main_code = m_dbutils.query_main_code(run["problem_id"],run["language"])
	delimiter = "USER_CODE_COMES_HERE"
	
	if driver_enabled == True:
		lines = main_code.split("\n")
		lines = map(lambda s: s.strip(), lines)
		line_no = lines.index("USER_CODE_COMES_HERE")
		global no_of_lines_to_subtract
		no_of_lines_to_subtract = line_no

	source_code = main_code.replace(delimiter,user_source_code)
	return source_code

def writetofile(content,source_file_path):
	file = open(source_file_path,"w")
	file.write(content)
	file.close()

def parse_libsandbox_result(filelocation):
	f = open(filelocation)
	lines = f.readlines()
	f.close()
	result = dict()
	if len(lines) == 3 :
		result["status"] = lines[0][7:9]
		result["time_taken"] = max(1,int(lines[1][4:]))
		result["memory_taken"] = int(lines[2][4:])*1024
		return result
	else :
		print 'Error while Parsing LibSandBox result '
		return result
	
def execute(run,driver_enabled):

	HOME_PATH_ORIGINAL = "/home/tariq"
	HOME_PATH_DUMMY = "/home/dummy/"
	SANDBOX_LOCATION = os.path.join(HOME_PATH_ORIGINAL,"radha/judge1/","sandbox/sandbox_c_cpp.py")
	submission_directory = os.path.join(HOME_PATH_DUMMY,"submissions",str(run["run_id"]))
	print submission_directory
	path_of_executable_binary = os.path.join(submission_directory,"a.out")
	
	os.chdir(submission_directory)
	IO_FOLDER = "jigjig/data"
	INPUT_FOLDER_LOCATION = os.path.join(HOME_PATH_ORIGINAL,IO_FOLDER,str(run["problem_id"]))
	OUTPUT_FILE_TEMP_LOCATION  =  os.path.join(HOME_PATH_ORIGINAL,IO_FOLDER)
	
#	print INPUT_FOLDER_LOCATION
#	print OUTPUT_FILE_TEMP_LOCATION

	finalresult = dict()
	cnt  = 1
	print driver_enabled
	testcases = m_dbutils.query_test_case(run)

	
	for test in testcases:
		# Compile And Test or Submission ?
		if run["issubmission"] == True:
			input_file_name = "input" + str(test[testcase_id])+".in"
		else:
			input_file_name = "sample_input" + str(test["testcase_id"])+".in"

		current_input_file_location = os.path.join(INPUT_FOLDER_LOCATION,input_file_name)
		run_output_file_location = os.path.join(submission_directory,"output"+str(test["testcase_id"])+".out")
		sanbox_result_location = os.path.join(submission_directory,"log")

		result_file_location = os.path.join(submission_directory,"result.txt")
	#	print current_input_file_location

	#	print result_file_location
		os.system("sudo touch "+ run_output_file_location)
		os.system("sudo touch "+ sanbox_result_location)
		os.system("sudo touch "+ result_file_location)
		# Give Write Permission for this file to the dummy user
		os.system("sudo chmod 777 " + run_output_file_location)
		os.system("sudo chmod 777 " + sanbox_result_location)
		os.system("sudo chmod 777 " + result_file_location)
		execute_command = "sudo -u tariq /usr/bin/python " + SANDBOX_LOCATION + " " + str(test["time_limit"]) + " " + \
				str(test["memory_limit"]) + " " + path_of_executable_binary + " < " + current_input_file_location + \
				" > " + run_output_file_location + " 2>" +sanbox_result_location

		os.system(execute_command)
		
		result = parse_libsandbox_result(sanbox_result_location)
		
		# Verify Wrong Answer 
		if result["status"] == "OK":

			# if you have to check PASS/FAIL
			if driver_enabled == True:
				file = open(result_file_location)
				temp = file.readlines()
				if len(temp) == 0 :
					result["status"] = "WA"
				if len(temp) > 0 and temp[0].rstrip() != "PASS" :
					result["status"] = "WA"
			# if you have to compare raw output file
			else:
				answer_output_file_location = os.path.join(OUTPUT_FILE_TEMP_LOCATION,str(run["problem_id"]),"output"+str(test["testcase_id"])+".out")
				print answer_output_file_location
				diffStatement = "diff --ignore-all-space --ignore-blank-lines %s %s " %(run_output_file_location,answer_output_file_location)
				diffOutput = commands.getoutput(diffStatement)
				if diffOutput != '':
					result["status"] = "WA"
		
		# output = std_output
		f = open(run_output_file_location)
		result["output"] = f.read()
		result["testcase_no"] = cnt
		m_dbutils.update_each_testrun(run,result)
		finalresult = result

		if result["status"] != "OK":
			break
		cnt+=1

	# Remove Directory
	command = "rm -rf %s" % (submission_directory)
	os.system(command)
	print finalresult["output"]
	m_dbutils.update_submission(run,finalresult)

def compile(run,driver_enabled):

	# Create a Directory for the submission if not present
	RESTRICTED_HOME_PATH = "/home/dummy"
	submission_directory = os.path.join(RESTRICTED_HOME_PATH,"submissions",str(run["run_id"]))
	mkdir_command = "mkdir -p " + submission_directory
	if not os.path.exists(submission_directory):
		os.system(mkdir_command)
	os.chdir(submission_directory)

	# Create the source File 
	
	source_code = preparesourcecode(run,driver_enabled)
	source_file_path = os.path.join(submission_directory,str(run["problem_id"])+".cpp")	
	writetofile(source_code,source_file_path)
	compiler_error_log_location = os.path.join(submission_directory,"compiler_error_log")
	# Compile the Source_code
	compile_command = "sudo  g++ -w -static -pipe -lm -s -fomit-frame-pointer -o %s %s 2>%s" %(submission_directory+"/a.out",source_file_path,compiler_error_log_location)

	temp = commands.getoutput(compile_command)

	# Adjust the lines if we have added some header for the user
	compiler_error_log=edit_compiler_log(compiler_error_log_location)
	
	if compiler_error_log == '':
		# Update DB saying 'COMPILATION SUCCESS'
		m_dbutils.update_compilation_status(run,"COMPILATION_SUCCESS"," ")
	else :
		
		# Update DB saying 'COMPILATION FAILURE'
		m_dbutils.update_compilation_status(run,"COMPILATION_ERROR",compiler_error_log)


def edit_compiler_log(compiler_error_log_location):

	global no_of_lines_to_subtract
	# Refer http://stackoverflow.com/questions/18022124/parsing-gcc-error-log
	awk_firstpart = "awk "
	awk_secondpart=   "'$4==\" warning\" || $4==\" note\" || $4==\" error\"{$2-=%s}1'" %(no_of_lines_to_subtract)
	awk_thirdpart = " FS=':' OFS=':' "
	awkcommand = awk_firstpart + awk_secondpart + awk_thirdpart + " " + compiler_error_log_location 
	edited_compiler_error_log = commands.getoutput(awkcommand)	
	return edited_compiler_error_log

def compile_and_run(run):
	
	driver_enabled = False
	driver_enabled = m_dbutils.query_is_driver_enabled(run["problem_id"])

	compile(run,driver_enabled)

	compilation_result = m_dbutils.query_compilation_result(run["run_id"])
	if compilation_result == "COMPILATION_SUCCESS":
		execute(run,driver_enabled)
